var isActive = false;

$('.bar').on('click', function () {
    if (isActive){
		$(this).removeClass('active');
		$('.nav-menu-mobile').removeClass('menu-open');
		$('.side-nav').removeClass('nav-overlay-open');
	} else{
		$(this).addClass('active');
		$('.nav-menu-mobile').addClass('menu-open');
		$('.side-nav').addClass('nav-overlay-open');
	}

	isActive = !isActive;
});

function openClose(){

}
/*
$(document).ready(function(){
  $('.side-nav').click(function(){
    $('.nav-menu-mobile').hide();
	$('.nav-overlay-open').hide();
  });
  $(".bar").click(function(){
    $(".nav-menu-mobile").show();
  });
});

<script>
  $('.menu-open').on('click', function (e) {
      var menu = $('.nav-overlay-open');
      if (!$(e.target).closest(menu).length) {
          menu.hide();
      }
  });
</script>
*/